#pragma once
#include <Arduino.h>
#include <FastLED.h>

class Stove {
    protected:
        String name = "";
        bool on = false;
        CRGB color = CRGB::Blue;
        const int BRIGHTNESS_MIN = 1;
        const int BRIGHTNESS_MAX = 255;
        int brightnessScaleSize = 5;
        int level = Stove::minLevel;
        int numberOfLeds;
        int minLevel = 1;
        int maxLevel = 5;
        int levelStep = 1;

    public:
        CRGB leds[1];

    public:
        Stove(int numberLeds, String uniqueName);

        String getName() { return this->name; };

        bool isOn() { return this->on; }
        bool toggle();
        bool toggle(bool status);
        bool getStatus() { return this->on; }
        int getLevel();
        int setLevel(int level);
        int getMaxLevel() { return this->maxLevel; }
        int getMinLevel() { return this->minLevel; }
        int getLevelStep() { return this->levelStep; }

        int getBrightness();
        CRGB getColor();

    protected:
        void log(String msg);
};