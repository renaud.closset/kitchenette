#include <Arduino.h>
#include <TM1637TinyDisplay.h>
#include <map>
#include <string>


class Display {

    private:
        TM1637TinyDisplay* display;
        const int8_t DOTS = 0b01000000;
        std::string flashText;
        std::string currentText;
        long flashEndTime;
        long timerEndTime;

    public:
        Display(int clockPin, int dataPin);
        long setFlashMessage(std::string message, int seconds = 4);
        long setFlashNumber(int number, int seconds = 4);
        long setTimerEndTime(long endTime);
        void service(long currentTime, int currentHours, int currentMinutes);

    private:
        void displayText(std::string text);
        void displayTimer(long timer);
        void displayTime(int hours, int minutes);
};