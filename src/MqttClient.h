#pragma once
#include <PubSubClient.h>

class MqttClient : public PubSubClient {
    
    using PubSubClient::PubSubClient;

    public:
      MqttClient(Client& client): PubSubClient(client){};
      bool publish(const char* topic, int payload, bool retain = false);
      bool publish(const char* topic, const char* payload, boolean retain);
};