#include "Oven.h"

Oven::Oven(int numberOfLeds, String uniqueName): Stove(numberOfLeds, uniqueName)
{
    this->color = CRGB::Red;
    this->minLevel = 50;
    this->maxLevel = 240;
    this->levelStep = 10;
}

unsigned long Oven::addSeconds(long seconds)
{
    if (seconds < 1) return this->endTime;
    if (this->endTime == 0) {
        return this->setSeconds(seconds);
    } else {
        return this->setEndTime(this->endTime + seconds * 1000 + 999);
    }
}

unsigned long Oven::setSeconds(long seconds)
{
    return this->setEndTime(millis() + (seconds * 1000) + 999);
}

unsigned long Oven::setEndTime(unsigned long endTime)
{
    unsigned long maxEndTime = millis() + (this->maxTime * 1000);
    this->endTime = max<long>(0, min(maxEndTime, endTime));
    Serial.printf("Setting endtime: %d\n", this->endTime);
    this->toggle(endTime > 0);
    return this->endTime;
}

void Oven::service(long currentTime)
{
    this->beep = false;
    if (this->endTime > 0 && this->endTime <= currentTime + 1001) {
        Serial.println("Oven timed ended");
        this->endTime = 0;
        this->toggle(false);
        this->beep = true;
    }
}