#include <Arduino.h>
#include <map>

class Buzzer {
    private:
        int buzzerPin;
        int currentState = LOW;
        long startAt = 0;
        const int beeps = 4;
        const int duration = 500;

    public:
        bool muted = false;

    public:
        Buzzer(int pin);
        void buzz();
        void service(long currentTime);
        int getTotalDuration() { return this->beeps * 2 * this->duration / 1000; }
};