#include <Arduino.h>
#include "Buzzer.h"
#include <map>

Buzzer::Buzzer(int pin) {
    buzzerPin = pin;
}

void Buzzer::buzz() {
    startAt = millis();
    Serial.print("Buzzer.START\t");
    Serial.println(startAt);
}

void Buzzer::service(long currentTime) {
    if (this->startAt == 0 || this->muted) return;

    long diff = currentTime - this->startAt;
    int step = (int) floor((float) diff / (float) this->duration) + 1;
    int status = step % 2;

    // it's over Jimmy
    if (step > (2 * this->beeps)) {
        this->startAt = 0;
        return;
    }
    
    if (this->currentState != status) {
        digitalWrite(this->buzzerPin, status);
        this->currentState = status;
    }
}