#include <Arduino.h>
#include "Stove.h"

Stove::Stove(int numberOfLeds, String uniqueName)
{
    this->name = uniqueName;
    this->numberOfLeds = numberOfLeds;
    this->leds[numberOfLeds];
    toggle(false);
}

bool Stove::toggle(bool status) {
    this->on = status;
    return this->on;
}

bool Stove::toggle() {
    return toggle(!this->on);
}

int Stove::setLevel(int level) {
    this->level = min(Stove::maxLevel, max(Stove::minLevel, level));
    this->on = this->level > 0;
    return this->level;
}

CRGB Stove::getColor()
{
    if (this->on) return this->color;
    return CRGB::Black;
}

int Stove::getBrightness()
{
    return (int) map(this->level, Stove::minLevel, Stove::maxLevel, Stove::BRIGHTNESS_MIN, Stove::BRIGHTNESS_MAX);
}

int Stove::getLevel()
{
    if (!this->on) return 0;
    return this->level;
}