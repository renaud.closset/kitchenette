#include "Display.h"
#include <TM1637TinyDisplay.h>
#include <map>
#include <string>

Display::Display(int clockPin, int dataPin)
{
    this->display = new TM1637TinyDisplay(clockPin, dataPin);
}

long Display::setFlashMessage(std::string message, int seconds)
{
    this->flashText = message;
    this->flashEndTime = millis() + (seconds * 1000);
    return this->flashEndTime;
}

long Display::setFlashNumber(int number, int seconds)
{
    char message[0];
    sprintf(message, "%d", number);
    return setFlashMessage(message, seconds);
}

long Display::setTimerEndTime(long endTime)
{
    if (this->timerEndTime != endTime) {
        this->flashEndTime = 0; // overwrite flash message on change
        this->timerEndTime = endTime;
    }
    return this->timerEndTime;
}

void Display::service(long currentTime, int currentHours, int currentMinutes)
{
    if (this->flashEndTime >= currentTime) {
        this->display->setBrightness(0x0f);
        displayText(this->flashText);
        return;
    }

    long timer = max((long) 0, this->timerEndTime - currentTime);
    if (timer > 0) {
        this->display->setBrightness(0x0f);
        displayTimer(timer);
        return;
    }

    if (currentHours < 25) {
        this->display->setBrightness(0x06);
        displayTime(currentHours, currentMinutes);
        return;
    }

    this->display->setBrightness(0x00, false);
    displayText("----");
}

void Display::displayText(std::string text)
{
    // we do not want animation, so capp text to 4 characters
    text = text.substr(0, 4);
    if (text == this->currentText) return;
    char* c = const_cast<char*>(text.c_str());
    display->showString(c);
    currentText = text;
}

void Display::displayTimer(long timer)
{
    display->showNumberDec((timer/1000 / 60) % 60, DOTS, true, 2, 0);
    display->showNumberDec(timer/1000 % 60, DOTS, true, 2, 2);
    this->currentText = "";
}

void Display::displayTime(int hours, int minutes)
{
    display->setBrightness(BRIGHT_LOW);
    display->showNumberDec(hours, DOTS, true, 2, 0);
    display->showNumberDec(minutes, DOTS, true, 2, 2);
    this->currentText = "";
}