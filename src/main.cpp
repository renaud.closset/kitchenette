#include <Arduino.h>
#include <TM1637TinyDisplay.h>
#include <FastLED.h>
#include <WiFiManager.h>
#include "Oven.h"
#include "Stove.h"
#include "Display.h"
#include "Buzzer.h"
#include "MqttClient.h"
#include <ArduinoOTA.h>
#include <ESPRotary.h>
#include <Button2.h>
#include <stdarg.h>
#include <ezTime.h>

#define HOST "esp-kitchenette"
#define WIFI_ENABLED true

/* DIGITAL PINS */

/* Rotary #1 (oven) */
#define PIN_ROT_1_CLK 21
#define PIN_ROT_1_DTA 22
#define PIN_ROT_1_SWT 23

/* Rotary #2 (stove1) */
#define PIN_ROT_2_CLK 5
#define PIN_ROT_2_DTA 18
#define PIN_ROT_2_SWT 19

/* Rotary #3 (stove2) */
#define PIN_ROT_3_CLK 4
#define PIN_ROT_3_DTA 16
#define PIN_ROT_3_SWT 17

/* Buzzer (timer) */
#define PIN_BUZZER 15

/* Display */
#define PIN_DSP_CLK 13 // TM1637 Clock
#define PIN_DSP_DIO 14 // TM1637 Data IO

/* Led #1 (oven) */
#define PIN_LED_1_DO 27

/* Led #2 (Stove1) */
#define PIN_LED_2_DO 26

/* Led #3 (Stove2) */
#define PIN_LED_3_DO 25


/* LEDS */
#define LED_1_NUM_LED 14
#define LED_2_NUM_LED 8
#define LED_3_NUM_LED 8

/* OVEN */
#define OVEN_TIMER_INCREASE 10

#define FLASH_MSG_DURATION 2

/* 
ezTime
https://github.com/jandrassy/ArduinoOTA?tab=readme-ov-file#esp8266-and-esp32-support
*/
Timezone myTimeZone;

/* MQTT */
#define MQTT_HOST "192.168.0.91"
#define MQTT_PORT 1883

WiFiClient espClient;
MqttClient mqttClient(espClient);

Oven oven(LED_1_NUM_LED, "oven");
Stove stove1(LED_2_NUM_LED, "left");
Stove stove2(LED_3_NUM_LED, "right");

Display display(PIN_DSP_CLK, PIN_DSP_DIO);
Buzzer buzzer(PIN_BUZZER);

CLEDController* ovenController;
CLEDController* stove1Controller;
CLEDController* stove2Controller;

ESPRotary ovenRotary(PIN_ROT_1_DTA, PIN_ROT_1_CLK, 2, oven.getMinLevel(), oven.getMaxLevel(), oven.getMinLevel(), oven.getLevelStep());
ESPRotary stove1Rotary(PIN_ROT_2_DTA, PIN_ROT_2_CLK, 2, stove1.getMinLevel(), stove1.getMaxLevel(), stove1.getMinLevel(), stove1.getLevelStep());
ESPRotary stove2Rotary(PIN_ROT_3_DTA, PIN_ROT_3_CLK, 2, stove2.getMinLevel(), stove2.getMaxLevel(), stove2.getMinLevel(), stove2.getLevelStep());

Button2 ovenButton;
Button2 stove1Button;
Button2 stove2Button;

bool wifiConnected = false;
bool mqttConnected = false;
bool isMuted = false;
long lastMqttReconnectAttempt = 0;

WiFiManager wm;
bool shouldSaveConfig = false;
void saveConfigCallback () {
  shouldSaveConfig = true;
}

void ovenTempChange(ESPRotary& r) {
  if (!oven.isOn()) return;
  int temperature = oven.setLevel(r.getPosition());
  mqttClient.publish("esp32-kitchenette/status/oven/temperature", temperature, true);
  char msg[4 + sizeof(char)];
  if (temperature >= 100) {
    std::sprintf(msg, "%dC", temperature);
  }else {
    std::sprintf(msg, " %dC", temperature);
  }
  display.setFlashMessage(msg, FLASH_MSG_DURATION);
}

void ovenButtonReleased(Button2& btn)
{
    oven.addSeconds(OVEN_TIMER_INCREASE);
}

void stove1Toggle(bool toggle)
{
  if (toggle == stove1.isOn()) return;
  stove1.toggle(toggle);
  if (stove1.isOn()) {
    char msg[4 + sizeof(char)];
    std::sprintf(msg, " %d ", stove1.getLevel());
    display.setFlashMessage(msg, FLASH_MSG_DURATION);
  }
  mqttClient.publish("esp32-kitchenette/status/stove/left", toggle ? "1" : "0", true);
}

void stove1Toggle()
{
  stove1Toggle(!stove1.isOn());
}

void stove1SetLevel(int level)
{
  int currentLevel = stove1.getLevel();
  if (level == currentLevel) return;
  level = stove1.setLevel(level);
  if (level == currentLevel) return;
  char msg[4 + sizeof(char)];
  std::sprintf(msg, "%d   ", level);
  display.setFlashMessage(msg, FLASH_MSG_DURATION);
  mqttClient.publish("esp32-kitchenette/status/stove/left/level", level, true);
}

void stove1RotaryChange(ESPRotary& r)
{
  if (!stove1.isOn()) return;
  int position = r.getPosition();
  stove1SetLevel(position);
}

void stove1ButtonReleased(Button2& btn)
{
  stove1Toggle();
}

void stove2Toggle(bool toggle)
{
  if (toggle == stove2.isOn()) return;
  stove2.toggle(toggle);
  if (stove2.isOn()) {
    char msg[4 + sizeof(char)];
    std::sprintf(msg, "   %d", stove2.getLevel());
    display.setFlashMessage(msg, FLASH_MSG_DURATION);
  }
  mqttClient.publish("esp32-kitchenette/status/stove/right", toggle ? "1" : "0", true);
}

void stove2Toggle()
{
  stove2Toggle(!stove2.isOn());
}

void stove2SetLevel(int level)
{
  int currentLevel = stove2.getLevel();
  if (level == currentLevel) return;
  level = stove2.setLevel(level);
  if (level == currentLevel) return;
  char msg[4 + sizeof(char)];
  std::sprintf(msg, "   %d", level);
  display.setFlashMessage(msg, FLASH_MSG_DURATION);
  mqttClient.publish("esp32-kitchenette/status/stove/right/level", level, true);
}

void stove2RotaryChange(ESPRotary& r)
{
  if (!stove2.isOn()) return;
  int position = r.getPosition();
  stove2SetLevel(position);
}

void stove2ButtonReleased(Button2& btn)
{
  stove2Toggle();
}


void mqttCallback(char *topic, byte *payload, unsigned int length)
{
  payload[length] = '\0';
  String s = String((char*) payload);

  Serial.printf("MQTT.message\t%s\t%s\n", topic, s.c_str());

  // oven
  if (strcmp(topic, "esp32-kitchenette/set/oven/timer") == 0) {
    oven.setSeconds(s.toInt());
    return;
  }
  if (strcmp(topic, "esp32-kitchenette/set/oven/temperature") == 0) {
    oven.setLevel(s.toInt());
    return;
  }

  // stove1
  if (strcmp(topic, "esp32-kitchenette/set/stove/left") == 0) {
    stove1Toggle(s.toInt()>0);
    return;
  }
  
  if (strcmp(topic, "esp32-kitchenette/set/stove/left/level") == 0) {
    bool on = s.toInt() > 0;
    stove1Toggle(on);
    if (on) stove1SetLevel(s.toInt());
    return;
  }

  // stove2
  if (strcmp(topic, "esp32-kitchenette/set/stove/right") == 0) {
    stove2Toggle(s.toInt()>0);
    return;
  }

  if (strcmp(topic, "esp32-kitchenette/set/stove/right/level") == 0) {
    bool on = s.toInt() > 0;
    stove2Toggle(on);
    if (on) stove2SetLevel(s.toInt());
    return;
  }

  if (strcmp(topic, "esp32-kitchenette/set/muted") == 0) {
    isMuted = s.toInt() > 0;
    display.setFlashMessage(isMuted ? "SOFF" : "S ON", 2);
    mqttClient.publish("esp32-kitchenette/status/muted", isMuted ? "1" : "0", true);
    return;
  }
}

void mqttConnect()
{
  mqttConnected = mqttClient.connect(HOST);
  Serial.printf("MQTT.connect:\t%s\t@ %s:%i\n", mqttConnected ? "OK" : "NOK", MQTT_HOST, MQTT_PORT);
  
  if(mqttConnected) {
      mqttClient.subscribe("esp32-kitchenette/set/#");
      
      mqttClient.publish("esp32-kitchenette/status/oven/temperature", oven.getLevel(), true);
      mqttClient.publish("esp32-kitchenette/status/oven/temperature/min", oven.getMinLevel(), true);
      mqttClient.publish("esp32-kitchenette/status/oven/temperature/max", oven.getMaxLevel(), true);

      mqttClient.publish("esp32-kitchenette/status/stove/left/level", stove1.getLevel(), true);
      mqttClient.publish("esp32-kitchenette/status/stove/left/level/min", stove1.getMinLevel(), true);
      mqttClient.publish("esp32-kitchenette/status/stove/left/level/max", stove1.getMaxLevel(), true);

      mqttClient.publish("esp32-kitchenette/status/stove/right/level", stove2.getLevel(), true);
      mqttClient.publish("esp32-kitchenette/status/stove/right/level/min", stove2.getMinLevel(), true);
      mqttClient.publish("esp32-kitchenette/status/stove/right/level/max", stove2.getMaxLevel(), true);
      
      mqttClient.publish("esp32-kitchenette/status/muted", isMuted ? "1" : "0", true);
    }
}

void connectToServices()
{
  // Arduino OTA
  Serial.println("ArduinoOTA.begin");
  ArduinoOTA.begin();

  // MQTT
  mqttConnect();

  // ezTime sync & timezone setting
  waitForSync();
  myTimeZone.setLocation("Europe/Brussels");
}

void setup()
{
  Serial.begin(9600);

  pinMode(PIN_ROT_1_CLK, INPUT);
  pinMode(PIN_ROT_1_CLK, INPUT_PULLUP);
  pinMode(PIN_ROT_1_DTA, INPUT);
  pinMode(PIN_ROT_1_DTA, INPUT_PULLUP);

  pinMode(PIN_ROT_2_CLK, INPUT);
  pinMode(PIN_ROT_2_DTA, INPUT);

  pinMode(PIN_ROT_3_CLK, INPUT);
  pinMode(PIN_ROT_3_DTA, INPUT);

  pinMode(PIN_BUZZER,  OUTPUT);

  ovenButton.begin(PIN_ROT_1_SWT);
  ovenButton.setReleasedHandler(ovenButtonReleased);

  stove1Button.begin(PIN_ROT_2_SWT);
  stove1Button.setReleasedHandler(stove1ButtonReleased);

  stove2Button.begin(PIN_ROT_3_SWT);
  stove2Button.setReleasedHandler(stove2ButtonReleased);

  ovenController =  &FastLED.addLeds<WS2812B, PIN_LED_1_DO, GRB>(oven.leds, LED_1_NUM_LED);
  stove1Controller = &FastLED.addLeds<WS2812B, PIN_LED_2_DO, GRB>(stove1.leds, LED_2_NUM_LED);
  stove2Controller = &FastLED.addLeds<WS2812B, PIN_LED_3_DO, GRB>(stove2.leds, LED_3_NUM_LED);

  ovenRotary.setChangedHandler(ovenTempChange);
  stove1Rotary.setChangedHandler(stove1RotaryChange);
  stove2Rotary.setChangedHandler(stove2RotaryChange);

  // OTA Updates
  ArduinoOTA.setHostname(HOST);
  ArduinoOTA.setPort(3232);


  // MQTT connection
  mqttClient.setServer(MQTT_HOST, MQTT_PORT);
  mqttClient.setCallback(mqttCallback);

  // WIFI
  if (WIFI_ENABLED) {
    wm.setConfigPortalBlocking(false);
    wm.setSaveConfigCallback(saveConfigCallback);
    wm.setHostname(HOST);
    wifiConnected = wm.autoConnect(HOST);
  }

  // Connect to NTP, MQTT & ArduioOTA
  if (wifiConnected) connectToServices();
}

void loop()
{
  ArduinoOTA.handle();

  long currentTime = millis();

  // manage wifi
  if (WIFI_ENABLED) {
    bool prevWifiConnected = wifiConnected;
    wm.process();
    wifiConnected = wm.getLastConxResult();
    if (!prevWifiConnected && wifiConnected) {
      Serial.println("Wifi.RECONNECTED");
      connectToServices(); // on reconnect
    }
    if (prevWifiConnected && !wifiConnected) {
      Serial.println("Wifi.DISCONNECTED");
      mqttConnected = false;
    }
  }

  // handle MQTT stuff
  if (!mqttClient.connected()) {
    if (currentTime - lastMqttReconnectAttempt > 5000) {
      lastMqttReconnectAttempt = currentTime;
      // Attempt to reconnect
      if (mqttClient.connect(HOST)) {
        lastMqttReconnectAttempt = 0;
      }
    }
  } else {
    mqttClient.loop();
  }

  // retrieve current time if possible
  int h = (wifiConnected) ? myTimeZone.hour() : 99;
  int m = (wifiConnected) ? myTimeZone.minute() : 99;
  display.service(currentTime, h, m);

  oven.service(currentTime);
  buzzer.service(currentTime);

  ovenRotary.loop();
  ovenButton.loop();

  stove1Rotary.loop();
  stove1Button.loop();

  stove2Rotary.loop();
  stove2Button.loop();

  display.setTimerEndTime(oven.getEndTime());

  if (oven.isBeep()) {
    buzzer.muted = (isMuted || !wifiConnected);
    buzzer.buzz();
    mqttClient.publish("esp32-kitchenette/status/oven/alarm", "", false);
    display.setFlashMessage("8EEP", buzzer.getTotalDuration());
  }

  ovenController->showColor(oven.getColor(), oven.getBrightness());
  stove1Controller->showColor(stove1.getColor(), stove1.getBrightness());
  stove2Controller->showColor(stove2.getColor(), stove2.getBrightness());
}