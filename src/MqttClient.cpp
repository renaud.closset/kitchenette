#include "MqttClient.h"
#include <PubSubClient.h>

bool MqttClient::publish(const char* topic, int payload, boolean retain)
{
    char const *num_char = std::to_string(payload).c_str();
    return this->publish(topic, num_char, retain);
}

bool MqttClient::publish(const char* topic, const char* payload, boolean retain)
{
    if (!this->connected()) return false;
    Serial.printf("MQTT.publish\t%s\t%s\n", topic, payload);
    return PubSubClient::publish(topic, payload, retain);
}