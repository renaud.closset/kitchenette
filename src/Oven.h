#pragma once
#include <Arduino.h>
#include <FastLED.h>
#include "Stove.h"

class Oven : public Stove{

    private:
        unsigned long endTime = 0;
        unsigned long maxTime = 119;
        bool beep = false;

    public:
        CRGB leds[10];

    public:

        Oven(int numberOfLeds, String uniqueName);

        long getEndTime() { return this->endTime; };

        unsigned long setSeconds(long seconds);
        unsigned long addSeconds(long seconds);

        void service(long currentTime);
        bool isBeep() { return this->beep; };

    private:
        unsigned long setEndTime(unsigned long endTime);
};